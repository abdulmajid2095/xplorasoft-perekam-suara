package com.xplorasoft.perekamsuara.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.xplorasoft.perekamsuara.R;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xplorasoft.perekamsuara.R;
import com.xplorasoft.perekamsuara.RecordingService;
import com.melnykov.fab.FloatingActionButton;
import com.xplorasoft.perekamsuara.fragments.RecordFragment;

import java.io.File;

public class Utama extends AppCompatActivity {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_POSITION = "position";
    private static final String LOG_TAG = RecordFragment.class.getSimpleName();

    private int position;

    //Recording controls
    private FloatingActionButton mRecordButton = null;
    private FloatingActionButton mStopButton = null;
    private Button mPauseButton = null;

    private TextView mRecordingPrompt;
    private int mRecordPromptCount = 0;

    private boolean mStartRecording = true;
    private boolean mPauseRecording = true;

    private Chronometer mChronometer = null;
    long timeWhenPaused = 0; //stores time when user clicks pause button

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Record_Fragment.
     */
    public static RecordFragment newInstance(int position) {
        RecordFragment f = new RecordFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);

        return f;
    }

    LinearLayout layRecord, layStop;

    ImageView menuku;
    Boolean open = false;
    DialogPlus dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_utama);

        layRecord = findViewById(R.id.layRecord);
        layStop = findViewById(R.id.layStop);

        YoYo.with(Techniques.SlideOutRight)
                .duration(1)
                .playOn(layStop);

        askPermission();

        mChronometer = (Chronometer) findViewById(R.id.chronometer);
        //update recording prompt text
        mRecordingPrompt = (TextView) findViewById(R.id.recording_status_text);

        mRecordButton = (FloatingActionButton) findViewById(R.id.btnRecord);
        mRecordButton.setColorNormal(getResources().getColor(R.color.primary));
        mRecordButton.setColorPressed(getResources().getColor(R.color.primary_dark));
        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                int permission2 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
                if(permission == PackageManager.PERMISSION_GRANTED && permission2 == PackageManager.PERMISSION_GRANTED)
                {
                    onRecord(mStartRecording);
                    mStartRecording = !mStartRecording;
                }
                else
                {
                    askPermission();
                }

            }
        });

        mStopButton = (FloatingActionButton) findViewById(R.id.btnStop);
        mStopButton.setColorNormal(getResources().getColor(R.color.primary));
        mStopButton.setColorPressed(getResources().getColor(R.color.primary_dark));
        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
                int permission2 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
                if(permission == PackageManager.PERMISSION_GRANTED && permission2 == PackageManager.PERMISSION_GRANTED)
                {
                    onRecord(mStartRecording);
                    mStartRecording = !mStartRecording;
                }
                else
                {
                    askPermission();
                }

            }
        });

        mPauseButton = (Button) findViewById(R.id.btnPause);
        mPauseButton.setVisibility(View.GONE); //hide pause button before recording starts
        mPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPauseRecord(mPauseRecording);
                mPauseRecording = !mPauseRecording;
            }
        });

        dialog();
        menuku = findViewById(R.id.menuku);
        menuku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mStartRecording)
                {
                    Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.turnOffFirst), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(open)
                    {
                        open = false;
                        menuku.setImageResource(R.drawable.ic_menu);
                        dialog.dismiss();
                    }
                    else
                    {
                        open = true;
                        menuku.setImageResource(R.drawable.ic_close);
                        dialog.show();
                    }
                }

            }
        });
    }

    // Recording Start/Stop
    //TODO: recording pause
    private void onRecord(boolean start){

        Intent intent = new Intent(getApplicationContext(), RecordingService.class);

        if (start) {
            YoYo.with(Techniques.SlideOutLeft)
                    .duration(811)
                    .playOn(layRecord);

                YoYo.with(Techniques.SlideInRight)
                        .duration(811)
                        .playOn(layStop);
            // start recording
            //mRecordButton.setImageResource(R.drawable.ic_media_stop);
            //mPauseButton.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(),R.string.toast_recording_start, Toast.LENGTH_SHORT).show();
            File folder = new File(Environment.getExternalStorageDirectory() + "/SoundRecorder");
            if (!folder.exists()) {
                //folder /SoundRecorder doesn't exist, create the folder
                folder.mkdir();
            }

            //start Chronometer
            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.start();
            mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                @Override
                public void onChronometerTick(Chronometer chronometer) {
                    if (mRecordPromptCount == 0) {
                        mRecordingPrompt.setText(getString(R.string.record_in_progress) + ".");
                    } else if (mRecordPromptCount == 1) {
                        mRecordingPrompt.setText(getString(R.string.record_in_progress) + "..");
                    } else if (mRecordPromptCount == 2) {
                        mRecordingPrompt.setText(getString(R.string.record_in_progress) + "...");
                        mRecordPromptCount = -1;
                    }

                    mRecordPromptCount++;
                }
            });

            //start RecordingService
            this.startService(intent);
            //keep screen on while recording
            this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            mRecordingPrompt.setText(getString(R.string.record_in_progress) + " .");
            mRecordPromptCount++;

        } else {
            //stop recording
                YoYo.with(Techniques.SlideInLeft)
                        .duration(811)
                        .playOn(layRecord);
                    YoYo.with(Techniques.SlideOutRight)
                            .duration(811)
                            .playOn(layStop);
            //mRecordButton.setImageResource(R.drawable.ic_mic_white_36dp);
            //mPauseButton.setVisibility(View.GONE);
            mChronometer.stop();
            mChronometer.setBase(SystemClock.elapsedRealtime());
            timeWhenPaused = 0;
            mRecordingPrompt.setText(getString(R.string.record_prompt));

            this.stopService(intent);
            //allow the screen to turn off again once recording is finished
            this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    //TODO: implement pause recording
    private void onPauseRecord(boolean pause) {
        if (pause) {
            //pause recording
            mPauseButton.setCompoundDrawablesWithIntrinsicBounds
                    (R.drawable.ic_media_play ,0 ,0 ,0);
            mRecordingPrompt.setText((String)getString(R.string.resume_recording_button).toUpperCase());
            timeWhenPaused = mChronometer.getBase() - SystemClock.elapsedRealtime();
            mChronometer.stop();
        } else {
            //resume recording
            mPauseButton.setCompoundDrawablesWithIntrinsicBounds
                    (R.drawable.ic_media_pause ,0 ,0 ,0);
            mRecordingPrompt.setText((String)getString(R.string.pause_recording_button).toUpperCase());
            mChronometer.setBase(SystemClock.elapsedRealtime() + timeWhenPaused);
            mChronometer.start();
        }
    }


    public void askPermission()
    {
        int permission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permission2 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        // If we don't have permissions, ask user for permissions

        if (permission2 != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(),"Ijinnkan Aplikasi untuk merekam suara",Toast.LENGTH_SHORT).show();
            String[] PERMISSIONS_AUDIO = {
                    Manifest.permission.RECORD_AUDIO
            };
            int REQUEST_AUDIO = 1;

            ActivityCompat.requestPermissions(
                    Utama.this,
                    PERMISSIONS_AUDIO,
                    REQUEST_AUDIO
            );
        }

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(),"Ijinnkan Aplikasi untuk menyimpan file",Toast.LENGTH_SHORT).show();
            String[] PERMISSIONS_STORAGE = {
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            };
            int REQUEST_EXTERNAL_STORAGE = 1;

            ActivityCompat.requestPermissions(
                    Utama.this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public void dialog()
    {
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_menu))
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View customView = dialog.getHolderView();

        LinearLayout file = customView.findViewById(R.id.file);
        file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                menuku.setImageResource(R.drawable.ic_menu);
                open = false;
                Intent intent = new Intent(Utama.this, RekamanTersimpan.class);
                startActivity(intent);
            }
        });

        LinearLayout setting = customView.findViewById(R.id.setting);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                menuku.setImageResource(R.drawable.ic_menu);
                open = false;
                Intent intent = new Intent(Utama.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout about = customView.findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                menuku.setImageResource(R.drawable.ic_menu);
                open = false;
                Intent intent = new Intent(Utama.this, Tentang.class);
                startActivity(intent);
            }
        });
    }
}
